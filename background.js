let sendRes = null
const url = 'https://savemystory.org/'

chrome.runtime.onMessageExternal.addListener(
  (request, sender, sendResponse) => {
    if (url === sender.url) {
      if (request) {
        if (request.message) {
          if (request.message === 'version') {
            sendResponse({ version: 1.0 })

            chrome.pageAction.show(sender.tab.id)
          }
          if (request.message === 'login') {
            sendRes = sendResponse
          }
        }
      }
    }

    return true
  })

chrome.runtime.onMessage.addListener(
  (request) => {
    if (sendRes) sendRes({ data: request })
    sendRes = null

    return true
  })
